var g_socketid = -1;

var adress;
var UUID;

function enableBT() {
	window.plugins.BluetoothPlugin.enable( function() {
		//alert( 'Enabling successfull' );
		discoverDevices();
	}, function(error) {
		alert( 'Error activando BT: ' + error );
	} );
}

function disableBT() {
	window.plugins.BluetoothPlugin.disable( function() {
		//alert( 'Disabling successfull' );
	}, function(error) {
		alert( 'Error disabling BT: ' + error );
	} );
}

function discoverDevices() {
    window.plugins.BluetoothPlugin.discoverDevices( function(devices) {
		for( var i = 0; i < devices.length; i++ ) {
    		if (devices[i].name=="pulsadorBlue"){
    			adress=devices[i].address;
    			break;
    		}    		
    	}
		listUUIDs();
    }, function(error) { alert( 'Error: No se ha encontrado el pulsador' ); } );
}

function listUUIDs() {
	window.plugins.BluetoothPlugin.getUUIDs( function(uuids) {
		for( var i = 0; i < uuids.length; i++ ) {
			UUID= uuids[i];
		}
		openRfcomm();
	}, function(error) { alert( 'Error UUID: ' + error ); }, adress );
}

function openRfcomm() {
	window.plugins.BluetoothPlugin.connect(
			function(socketId) {
				g_socketid = socketId;
				console.log( 'Socket-id: ' + g_socketid );
				readRfcomm();
				preferences.set("bluet", g_socketid, function() {
			        //alert("Successfully saved!");
			    }, function(error) {
			        alert("Error! " + JSON.stringify(error));
					});
			},
			function(error) {
				alert( 'Error OPEN: ' + error );
			},
			adress,
			UUID
	);
	
}

function readRfcomm() {
	window.plugins.BluetoothPlugin.read( bp_readSuccess, bp_readError, g_socketid );
}

function readRfcommParam(sock) {
	g_socketid=sock;
	readRfcomm();
}

function bp_readError( error ) {
	alert( 'Error READ: ' + error );
}

function bp_readSuccess( p_data ) {
		jQuery("body").trigger("click");
	// Continue reading...
	window.plugins.BluetoothPlugin.read( bp_readSuccess, bp_readError, g_socketid );
	
	return;
}


