// CONSTANTS
// States
const SELECT_ROW = 0;
const SELECT_COLUMN = 1;
const START = 2;
const RESTART = 3;
const NO_ACTION = 4;
const GOTO = 5;

const INTERVALO = 2000;
const TIME_BETWEEN_GOTOS = 800;

const MAX_ITERATION = 20;

// GLOBAL VARIABLES
var state = START;
var cells;
var currentColumn, previousColumn; 
var currentRow, previousRow;
var previousColor;
var handlerBarrido;
var nIteration;
var mostrarPulsador = true;


function getValueCell() {
	return $(cells[currentRow][previousColumn]).children(".box-text").text();
}

function nextCell() {
	var i,j;

	i=0;
	for(j=0;j<cells[i].length;j++) {
		$(cells[i][j]).css({'background':'#2ECCFA'});
		//$(cells[j]).fadeTo(0,0.8);
	}			
	
	for(i=1;i<cells.length;i++) {
		for(j=0;j<cells[i].length;j++) {
			$(cells[i][j]).css({'background':'#E8E8E8'});
			//$(cells[j]).fadeTo(0,0.8);
		}			
	}
	
	$(cells[currentRow][currentColumn]).css({'background':'#F3F781'});
	currentColumn = (currentColumn+1)%cells[currentRow].length;
	if (currentColumn == 0) {
		currentRow = (currentRow+1)%cells.length;	
	}
}

function nextRow() {
	var j;
	
	for(j=0;j<cells[previousRow].length;j++) {
		$(cells[previousRow][j]).css({'background':previousColor,'border-color':'red'});
	}

	previousColor = $(cells[currentRow][0]).css("background-color");
	for(j=0;j<cells[currentRow].length;j++) {
		$(cells[currentRow][j]).css({'background':'#F3F781','border-color':'#33FF00'});
	}

	previousRow = currentRow;
	currentRow = (currentRow+1)%cells.length;
	
	checkInterval();
}

function nextColumn() {
	var j;
	
	currentRow = previousRow;
	console.log("Row "+currentRow);
	
	for(j=0;j<cells[currentRow].length;j++) {
		$(cells[currentRow][j]).css({'background':previousColor,'border-color':'red'});
	}
	
	$(cells[currentRow][currentColumn]).css({'background':'#F3F781','border-color':'#33FF00'});
	
	previousColumn = currentColumn;
	currentColumn = (currentColumn+1)%cells[currentRow].length;
	
	checkInterval();
}

function readElements() {
	cells = new Array();
	cells[currentRow] = new Array();
	
	$("#barraPrincipal .boxTop").each(function(index) {
		cells[currentRow].push(this);
	});
	
	$("#barraSecundaria .bar").each(function(index) {
		currentRow++;
		cells[currentRow] = new Array();
		$(this).children("div").each(function(index2) {
			cells[currentRow].push(this);
		});
	});
};

function startScanning() {
	state = SELECT_ROW;
	
	currentRow = 0;
	currentColumn = 0;
	previousRow = 0;
	
	handlerBarrido = setInterval(function(){nextRow();},INTERVALO);
};

function checkInterval() {
	if (nIteration >= MAX_ITERATION){
		state = NO_ACTION;
		switchState();
	} else {
		nIteration++;
	}
}

function restart() {
	clearInterval(handlerBarrido);
	for(j=0;j<cells[previousRow].length;j++) {
		$(cells[previousRow][j]).css({'background':previousColor,'border-color':'red'});
		previousColor = $(cells[previousRow][0]).css("background-color");	
	}
	
	currentRow = 0;
	currentColumn = 0;
	previousRow = 0;
	previousColumn = 0;
	nIteration = 0;
}

function switchState() {
	switch (state) {
	case (SELECT_ROW): 
		clearInterval(handlerBarrido);
		state = SELECT_COLUMN;
		nIteration = 0;
		handlerBarrido = setInterval(function(){nextColumn();},INTERVALO);
		break;
		
	case (SELECT_COLUMN):
		clearInterval(handlerBarrido);
		value = getValueCell();
		console.log(value);
		$("#previewBox").css({'visibility':'visible'});
		$("#previewBox").html($(cells[previousRow][previousColumn]).html());
		$("#previewBox").attr('class','boxSelected');
		$("#previewBox").css({'background-color':'#00ff00','border-color':'#00cc00'});
		state = GOTO;
		mostrarPulsador = false;

		setTimeout(function(){switchState();},TIME_BETWEEN_GOTOS);
		break;
		
	case (START):
		currentRow = 0;
		currentColumn = 0;
		previousRow = 0;
		previousColumn = 0;
		nIteration = 0;
		
		$("#pulsador").hide();
		readElements();
		startScanning();
		break;
		
	case (RESTART):
		restart();
		
		state = START;
		break;
		
	case (NO_ACTION):
		$("#pulsador").show();
		state = RESTART;
		switchState();
	break;
	
	case (GOTO):
		nIteration = 0;
		restart();
		state = START;
		switchState();
	break;
	
	default:
		alert("Incorrect State");
		break;
	}
}

function startBarrido() {
	state = START;
}

$(document).ready(function() {
	$("body").click(function() {
		switchState();
	});
});