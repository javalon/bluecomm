//BlueCom - Pulsador
// Configuracion del modulo bluetooth y deteccion de la pulsacion

int contador=1;
const int ledInd = 13;
const int entPulsador = 6;

//Sistema antirebote del pulsador
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 250;    // the debounce time; increase if the output flickers
int buttonState;
int lastButtonState = LOW;

void setup()
{
  //Led 13 para indicar final de operacion de configuracion AT
  pinMode(ledInd,OUTPUT);
  //Entrada 6 del pulsador
  pinMode(entPulsador,INPUT);
  //Velocidad del modulo bluetooth, 9600 por defecto
  Serial.begin(9600);
  //Apagamos el led 13
  digitalWrite(ledInd,LOW);
}

void loop()
{
  //Es para realizar los cambios una sola vez
  while (contador==1)
  {
    //Indicacion de tiempo de espera iniciado
    digitalWrite(ledInd,HIGH);
    /*Tiempo de espera de 15 segundos (prudencial, se puede cambiar, depende de lo que tardes
     en volver a conectarlos) para reconectar cables RX y TX del modulo bluetooth
     a la placa Arduino ya que para programar esta deben estar desconectados*/
    delay(15000);
    //Indicacion de tiempo de espera finalizado
    digitalWrite(ledInd,LOW);
    //Iniciamos comunicacion con modulo bluetooth mediante comandos AT
    Serial.print("AT");
    //Espera de 1 segundo según datasheet entre envio de comandos AT
    delay(1000);
    //Cambio de nombre donde se envia AT+NAME y seguido el nombre que deseemos
    Serial.print("AT+NAMEpulsadorBlue");
    //Espera de 1 segundo según datasheet entre envio de comandos AT
    delay(1000);
    /*Cambio de la velocidad del modulo en baudios
     Se envia AT+BAUD y seguido el numero correspondiente:
     
     1 --> 1200 baudios 
     2 --> 2400 baudios
     3 --> 4800 baudios
     4 --> 9600 baudios (por defecto)
     5 --> 19200 baudios
     6 --> 38400 baudios
     7 --> 57600 baudios
     8 --> 115200 baudios
     
     */
    Serial.print("AT+BAUD4");
    //Espera de 1 segundo según datasheet entre envio de comandos AT
    delay(1000);
    //Configuracion Password, se envia AT+PIN y seguido password que queremos
    Serial.print("AT+PIN1234");
    //Espera de 1 segundo según datasheet entre envio de comandos AT
    delay(1000);
    //Mostramos tanto por puerto serial y por led la finalizacion de la
    //configuracion AT del modulo bluetooth
    Serial.print("OK Cambios Realizados correctamente");
    digitalWrite(ledInd,HIGH);

    //Al tener contador=2 ya no se vuelve a repetir el while, a no ser que
    //se produzca un reset, por tanto comenzaria un nuevo cambio de configuracion
    contador=2;
  }

  delay(2000);
  //Ahora realizamos el bucle de deteccion de pulsacion y envio del mensaje 
  // al monitor de la tablet/movil

  int pulsado =0;
  digitalWrite(ledInd,LOW);

  while(Serial.available())
  {
    buttonState = digitalRead(entPulsador);

    // compare the buttonState to its previous state
    if (buttonState != lastButtonState) {
      if (buttonState == HIGH) {
        Serial.println("Pulsado$");
      } 
    }
    // save the current state as the last state, 
    //for next time through the loop
    lastButtonState = buttonState;
    digitalWrite(ledInd, buttonState);
    delay(100);
  }
}





